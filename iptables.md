# :fire: iptablesの使い方

- 【書式】

      iptables [-t <table>] <command> [match] [target/jump]

- 【参考URL】  
  <https://qiita.com/Tocyuki/items/6d90a1ec4dd8e991a1ce>

## ・テーブル

1. **filterテーブル**  
   ・・・パケットの通過や遮断の制御を行う  

       ex) iptables -t filter -p tcp --dport 80 -j DROP  
           iptables -p tcp --dport 80 -j DROP  
   ※filterテーブルはデフォルトのため省略可

2. **natテーブル**  
   ・・・natを担当  

       ex) iptables -t nat -A POSTROUTING -s 192.168.1.0/24 -j MASQUERADE  

3. **mangleテーブル**  
   ・・・TOSフィールド等の値を書き換える

       ex) iptables -t mangle -A PREROUTING -p tcp --dport 80 -j TOS --set-tos Maximize-Throughput  

4. **rawテーブル**  
   ・・・追跡を除外するようマークすることで、特定の通信をファイアーウォールで処理せずに他の機材へ通したりといった経路制御する場合に利用する  

       ex) iptables -t row -I PREROUTING -p udp --dport 53 -j NOTRACK  
           iptables -t row -I OUTPUT -p udp --dport -j NOTRACK  
   ※UDPポート53のパケット受信時と送信時にNOTRACKマークをつける  

## ・チェイン

1. **INPUT**  
   ・・・入力（受信）に対するチェイン
2. **OUTPUT**  
   ・・・出力（送信）に対するチェイン
3. **FORWARD**  
   ・・・フォワード（転送）に対するチェイン
4. **PREROUTING**  
   ・・・受信時に宛先アドレスを変換するチェイン  
   ※filterルール適用前
5. **POSTROUTING**  
   ・・・送信時に送信元アドレスを変換するチェイン  
   ※filterルール適用後  

## ・テーブルとチェインの関係

※filterとnatのみ  
・filterテーブル・・・INPUT,OUTPUT,FORWARD  
・natテーブル・・・POSTROUTING,PREROUTINT,OUTPUT  

## ・コマンド

1. 「**-A(--append**」  
   ・・・指定チェインに1つ以上の新規ルールを追加  
2. 「**-D(--delete)**」  
   ・・・指定チェインから1つ以上のルールを削除　　
3. 「**-P(--policy)**」  
   ・・・指定チェインのポリシーを指定したターゲットに設定
4. 「**-N(--new-chain)**」  
   ・・・新しいユーザー定義チェインを作成
5. 「**-X(--delete-chain)**」  
   ・・・指定ユーザー定義チェインを削除
6. 「**-I(--insert)**」  
   ・・・指定したチェインにルール番号を指定してルールを挿入  
     ※ルール番号を省略した場合はチェインの先頭に挿入される  

## ・パラメータ

1. 「**-s(--source)**」  
   ・・・パケットの送信元指定
2. 「**-d(--destination)**」  
   ・・・パケットの宛先指定
3. 「**-p(--protocol)**」  
   ・・・チェックするプロトコル指定  
   　※指定できるのは「tcp」「udp」「icmp」「all」またはプロトコル番号
4. 「**-i(--in-interface)**」  
   ・・・入力インターフェース  
5. 「**-o(--out-interface)**」  
   ・・・出力インターフェース  
6. 「**-j(--jump)**」  
   ・・・ターゲット（ACCEPT,DROP,REJECT）を指定  

## ・「-j」で指定するターゲット

1. **ACCEPT**・・・パケットの通過許可  
2. **DROP**・・・パケットを破棄。応答も返さない
3. **REJECT**・・・パケットを拒否し、ICMPメッセージを返信
4. **REDIRECT**・・・特定ポートにリダイレクト
5. **LOG**・・・マッチしたパケットのログを記録

## ・拡張パラメータ

1. 「**--sport**」  
   ・・・送信元ポート番号
2. 「**--dport**」  
   ・・・宛先ポート番号
3. 「**--tcp-flag**」  
   ・・・第1引数に評価されるTCPフラグを指定し、第2引数に設定されていなければならないフラグを指定する。  
4. 「**--syn**」  
   ・・・SYNビットがセットされた、
いわゆるTCPの接続確立時に、正規のスリーハンドシェイク手順を踏んだ通信を指定
5. 「**--icmp-type**」  
   ・・・ICMP のタイプを指定。pingに限定した指定をする際などに指定
